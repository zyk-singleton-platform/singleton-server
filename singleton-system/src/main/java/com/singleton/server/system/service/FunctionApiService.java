package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.system.controller.functionapi.FunctionApiNewRequ;
import com.singleton.server.system.entity.FunctionApi;

import java.util.List;

/**
 * <p>
 * 系统-功能api关系表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
public interface FunctionApiService extends IService<FunctionApi> {
    Boolean addOrUpdateBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long functionId);
    Boolean addBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long functionId);
    Boolean deletedBatchFunctionApiByFunctionId(Long functionId);
    Boolean updateBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long updateId);

    List<FunctionApi> getFunctionApisByFunctionIds(List<Long> functionIds);
}
