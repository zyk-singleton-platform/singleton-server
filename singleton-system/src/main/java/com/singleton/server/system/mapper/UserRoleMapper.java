package com.singleton.server.system.mapper;

import com.singleton.server.system.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统-用户角色关联表 Mapper 接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
