package com.singleton.server.system.enums;

import com.singleton.server.common.basic.BasicErrorCode;
import com.singleton.server.common.enums.ModuleEnum;

public enum SystemErrorCodeEnum implements BasicErrorCode {
    ARRGUMENT_ERROR(100,"参数错误");

    private final int code;

    private final String desc;

    private final int moduleCode = ModuleEnum.SYSTEM.getCode();

    SystemErrorCodeEnum(int code, String desc) {
        this.code = ERROR_CODE_LEVEL * moduleCode + code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return desc;
    }
}
