package com.singleton.server.system.component;

import com.singleton.server.common.utils.OptionalPlus;
import com.singleton.server.system.controller.role.RoleInfosResp;
import com.singleton.server.system.dto.RoleDTO;
import com.singleton.server.system.entity.FunctionApi;
import com.singleton.server.system.entity.RoleFunction;
import com.singleton.server.system.service.FunctionApiService;
import com.singleton.server.system.service.RoleFunctionService;
import com.singleton.server.system.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RolePermissionComponent  implements ApplicationRunner {
    private RoleService roleService;
    private FunctionApiService functionApiService;
    private RoleFunctionService roleFunctionService;

    @Autowired
    public RolePermissionComponent(RoleService roleService, FunctionApiService functionApiService, RoleFunctionService roleFunctionService) {
        this.roleService = roleService;
        this.functionApiService = functionApiService;
        this.roleFunctionService = roleFunctionService;
    }
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        this.init();
        log.info("RolePermissionComponent init success");
    }
    private final static int REFRESH_COUNT = 1000;
    private final ConcurrentHashMap<String, List<String>> mapRole2Permission = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, AtomicLong> mapRole2Index = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Long> mapRole2RefreshTime = new ConcurrentHashMap<>();
    private final ExecutorService asyncExecutor = new ThreadPoolExecutor(1
            , 1
            , 0L
            , TimeUnit.MILLISECONDS
            , new LinkedBlockingQueue<Runnable>(1000)
            , Executors.defaultThreadFactory()
            , new ThreadPoolExecutor.DiscardPolicy());

    public Boolean init(){
        List<RoleInfosResp> roleInfosResps = roleService.listAll();
        List<Long> roleIds = roleInfosResps.stream().map(RoleInfosResp::getId).collect(Collectors.toList());
        List<RoleFunction> roleFunctions = roleFunctionService.getRoleFunctionsByRoleIds(roleIds);
        List<Long> functionIds = roleFunctions.stream().map(RoleFunction::getFunctionId).collect(Collectors.toList());
        List<FunctionApi> functionApis = functionApiService.getFunctionApisByFunctionIds(functionIds);
        if(CollectionUtils.isEmpty(functionApis)){
            return Boolean.TRUE;
        }
        Map<Long, List<String>> mapFunctionId2ApiUrls = functionApis.stream().collect(Collectors.groupingBy(FunctionApi::getFunctionId, Collectors.mapping(FunctionApi::getApiUrl, Collectors.toList())));
        Map<Long, List<String>> mapRoleId2ApiUrls = roleFunctions.stream().collect(
                Collectors.toMap(RoleFunction::getRoleId, roleFunction -> OptionalPlus.ofNullable(mapFunctionId2ApiUrls.get(roleFunction.getFunctionId())).orElse(new ArrayList<>()), (A, B) -> {
            if(A == null){
                A = new ArrayList<>();
            }
            if(B == null){
                B = new ArrayList<>();
            }
            A.addAll(B);
            return A;
        }));
        for (RoleInfosResp roleInfosResp : roleInfosResps) {
            Long roleId = roleInfosResp.getId();
            String roleCode = roleInfosResp.getCode();
            List<String> apiUrls = mapRoleId2ApiUrls.get(roleId);
            if(apiUrls == null){
                apiUrls = new ArrayList<>();
            }
            mapRole2Permission.put(roleCode, new CopyOnWriteArrayList<>(apiUrls.stream().distinct().collect(Collectors.toList())));
        }
        return Boolean.TRUE;
    }

    public List<String> getPermissions(List<String> roleCodes) {
        if (CollectionUtils.isEmpty(roleCodes)) {
            return Collections.EMPTY_LIST;
        }
        ArrayList<String> permissions = new ArrayList<>();
        for (String roleCode : roleCodes) {
            mapRole2Index.computeIfAbsent(roleCode, key -> new AtomicLong(0L));
            mapRole2Permission.computeIfAbsent(roleCode, key -> new CopyOnWriteArrayList());
            long index = mapRole2Index.get(roleCode).incrementAndGet();
            if (index % REFRESH_COUNT == 0) {
                AsyncRefreshPermissions(roleCode);
            }
            List<String> apiUrls = mapRole2Permission.get(roleCode);
            if (index == 1 && apiUrls.size() == 0) {
                AsyncRefreshPermissions(roleCode);
            }
            permissions.addAll(apiUrls);
        }
        return permissions.stream().distinct().collect(Collectors.toList());
    }

    void AsyncRefreshPermissions(String roleCode) {
        CompletableFuture.supplyAsync(() -> refreshPermissions(roleCode), asyncExecutor);
    }

    private String refreshPermissions(String roleCode) {
        long l = System.currentTimeMillis();
        Long compute = mapRole2RefreshTime.compute(roleCode, (key, value) -> OptionalPlus.ofNullable(value).orElse(l));
        if(l-compute < 1000L){
            return "SUCCCESS";
        }
        log.info("RolePermissionComponent refreshPermissions star");
        List<RoleDTO> roleDTOS = roleService.getRolesByRoleCode(roleCode);
        List<Long> roleIds = roleDTOS.stream().map(RoleDTO::getId).collect(Collectors.toList());
        List<RoleFunction> roleFunctions = roleFunctionService.getRoleFunctionsByRoleIds(roleIds);
        List<Long> functionIds = roleFunctions.stream().map(RoleFunction::getFunctionId).collect(Collectors.toList());
        List<FunctionApi> functionApis = functionApiService.getFunctionApisByFunctionIds(functionIds);
        if(CollectionUtils.isEmpty(functionApis)){
            return "SUCCCESS";
        }
        List<String> apiUrls = functionApis.stream().map(FunctionApi::getApiUrl).distinct().collect(Collectors.toList());
        mapRole2RefreshTime.put(roleCode, System.currentTimeMillis());
        mapRole2Permission.put(roleCode, apiUrls);
        log.info("RolePermissionComponent refreshPermissions success");
        return "SUCCCESS";
    }
}
