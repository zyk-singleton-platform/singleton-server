package com.singleton.server.system.mapper;

import com.singleton.server.system.entity.Function;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统-权限功能表 Mapper 接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-28
 */
@Repository
public interface FunctionMapper extends BaseMapper<Function> {

}
