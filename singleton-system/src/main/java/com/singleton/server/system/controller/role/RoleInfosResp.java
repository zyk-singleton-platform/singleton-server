package com.singleton.server.system.controller.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 角色信息响应
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@ApiModel("角色信息响应")
public class RoleInfosResp implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("角色名称")
    private String name;

    @ApiModelProperty("角色代码")
    private String code;

    @ApiModelProperty("角色类型（0：业务角色，1: 管理角色）")
    private Integer type;

    @ApiModelProperty("是否可用")
    private Integer enable;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("功能ID")
    private List<Long> functionIds;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("已删除 1-已删除 0-未删除")
    private Boolean deleted;
}
