package com.singleton.server.system.service.impl;

import com.singleton.server.system.entity.Platform;
import com.singleton.server.system.mapper.PlatformMapper;
import com.singleton.server.system.service.PlatformService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统-平台表 服务实现类
 * </p>
 *
 * @author zhouyk
 * @since 2021-11-22
 */
@Service
public class PlatformServiceImpl extends ServiceImpl<PlatformMapper, Platform> implements PlatformService {

}
