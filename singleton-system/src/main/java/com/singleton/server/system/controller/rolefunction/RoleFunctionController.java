package com.singleton.server.system.controller.rolefunction;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统-角色功能表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-10-08
 */
@RestController
@RequestMapping("/system/role-function")
public class RoleFunctionController {

}
