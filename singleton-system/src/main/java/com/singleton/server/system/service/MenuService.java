package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.system.controller.menu.*;
import com.singleton.server.system.entity.Menu;

import java.util.List;

/**
 * <p>
 * 系统-权限功能表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-28
 */
public interface MenuService extends IService<Menu> {

    List<MenuInfosResp> listAll(String platformCode);

    List<Menu> list(String platformCode);

    List<MenuInfosResp> tree(String platformCode);

    Boolean syncMenuInfos(List<MenuSyncRequ> menuSyncRequs, String platformCode);

    List<Menu> getMenuTreeNodesById(Long menuId, String platformCode);

    List<MenuInfosResp> treeByUserId(Long userId, String platformCode);
}
