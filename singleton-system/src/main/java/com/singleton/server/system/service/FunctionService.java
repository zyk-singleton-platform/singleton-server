package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.system.controller.function.FunctionDeletedRequ;
import com.singleton.server.system.controller.function.FunctionEditedRequ;
import com.singleton.server.system.controller.function.FunctionInfosResp;
import com.singleton.server.system.controller.function.FunctionNewRequ;
import com.singleton.server.system.entity.Function;

import java.util.List;

/**
 * <p>
 * 系统-权限功能表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-28
 */
public interface FunctionService extends IService<Function> {

    Boolean addFunction(FunctionNewRequ functionNewRequ);

    List<FunctionInfosResp> listAll(String platformCode);

    List<FunctionInfosResp> listByMenuId(Long menuId, String platformCode);

    Boolean edited(FunctionEditedRequ functionEditedRequ);

    Boolean deleted(FunctionDeletedRequ functionDeletedRequ);

    Boolean deletedByMenuId(Long menuId);

    List<Function> listByIds(List<Long> functionIds, String platformCode);
}
