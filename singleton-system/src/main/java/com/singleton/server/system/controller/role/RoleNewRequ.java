package com.singleton.server.system.controller.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("角色新增请求")
public class RoleNewRequ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("角色名称")
    @NotEmpty(message = "角色名称不能为空!")
    private String name;

    @ApiModelProperty("角色代码")
    @NotEmpty(message = "角色代码不能为空!")
    private String code;

    @ApiModelProperty("角色类型（0：业务角色，1: 管理角色）")
    private Integer type = 0;

    @ApiModelProperty("功能ID")
    @NotEmpty(message = "功能ID不能为空!")
    private List<Long> functionIds;

    @ApiModelProperty("备注")
    private String remark;
}
