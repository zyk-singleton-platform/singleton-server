package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.system.entity.RoleFunction;

import java.util.List;

/**
 * <p>
 * 系统-角色功能表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
public interface RoleFunctionService extends IService<RoleFunction> {

    Boolean removeRoleFunctionByRoleId(Long roleId);

    List<RoleFunction> getRoleFunctionsByRoleIds(List<Long> roleIds);
}
