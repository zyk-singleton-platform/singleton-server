package com.singleton.server.system.controller.platform;


import com.singleton.server.common.result.Result;
import com.singleton.server.common.utils.BeanCopyUtil;
import com.singleton.server.system.entity.Platform;
import com.singleton.server.system.service.PlatformService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 系统-平台表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-11-22
 */

@Api(tags = "平台-API")
@RestController
@RequestMapping("/system/platform")
public class PlatformController {

    @Autowired
    private PlatformService platformService;

    @ApiOperation("列表")
    @GetMapping("/list")
    public Result<List<PlatformsResp>> list(){
        List<Platform> list = platformService.list();
        if(list == null){
            return Result.ok(Collections.emptyList());
        }
        return Result.ok(BeanCopyUtil.copyListProperties(list, PlatformsResp::new));
    }

}
