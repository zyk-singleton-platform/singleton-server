package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.system.entity.UserRole;

import java.util.List;

/**
 * <p>
 * 系统-用户角色关联表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
public interface UserRoleService extends IService<UserRole> {
    UserRole getUserRoleByRoleId(Long roleId);

    List<UserRole> getUserRolesByUserIds(List<Long> userIds);

    Boolean deletedUserRoleByUserId(Long userId);

    Boolean UpdateBatch(List<UserRole> lstUserRole, Long userId);
}
