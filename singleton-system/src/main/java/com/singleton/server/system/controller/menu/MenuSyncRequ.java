package com.singleton.server.system.controller.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("菜单同步")
public class MenuSyncRequ implements Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("平台CODE")
    @NotEmpty(message = "平台CODE不能为空!")
    private String platformCode;

    @ApiModelProperty("位置")
    private Long treeIndex;

    @ApiModelProperty("父位置")
    private Long pIndex;

    @ApiModelProperty("路由Key")
    @NotEmpty(message = "路由Key不能为空!")
    private String routeKey;

    @ApiModelProperty("菜单名称")
    @NotEmpty(message = "菜单名称不能为空!")
    private String title;

    @ApiModelProperty("排序")
    @NotNull(message = "排序不能为空!")
    private Integer sort;

    @ApiModelProperty("元数据")
    private String meta;

    @Valid
    @ApiModelProperty("子菜单列表")
    private List<MenuSyncRequ> childrens;

}
