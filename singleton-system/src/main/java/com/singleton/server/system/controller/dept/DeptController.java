package com.singleton.server.system.controller.dept;


import com.singleton.server.common.result.Result;
import com.singleton.server.system.service.DeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统-部门表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-10-08
 */
@Api(tags = "部门信息")
@RestController
@RequestMapping("/system/dept")
public class DeptController {
    @Autowired
    private DeptService deptService;

    @ApiOperation("列表")
    @GetMapping("/list")
    public Result<List<DeptInfosResp>> list(){
        List<DeptInfosResp> list = deptService.listAll();
        return Result.ok(list);
    }

    @ApiOperation("新增")
    @PostMapping("/new")
    public Result<Long> newsDept(@RequestBody @Validated DeptNewRequ deptNewRequ){
        Long id = deptService.addDept(deptNewRequ);
        return Result.ok(id);
    }

    @ApiOperation("更新")
    @PostMapping("/edited")
    public Result<Boolean> edited(@RequestBody @Validated DeptEditedRequ deptEditedRequ){
        Boolean aBoolean = deptService.edited(deptEditedRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("删除")
    @PostMapping("/deleted")
    public Result<Boolean> deleted(@RequestBody @Validated DeptDeletedRequ deptDeletedRequ){
        Boolean aBoolean = deptService.deleted(deptDeletedRequ);
        return Result.ok(aBoolean);
    }
}
