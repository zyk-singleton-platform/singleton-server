package com.singleton.server.system.service;

import com.singleton.server.system.controller.dept.DeptDeletedRequ;
import com.singleton.server.system.controller.dept.DeptEditedRequ;
import com.singleton.server.system.controller.dept.DeptInfosResp;
import com.singleton.server.system.controller.dept.DeptNewRequ;
import com.singleton.server.system.entity.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统-部门表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
public interface DeptService extends IService<Dept> {

    List<DeptInfosResp> listAll();

    Long addDept(DeptNewRequ deptNewRequ);

    Boolean edited(DeptEditedRequ deptEditedRequ);

    Boolean deleted(DeptDeletedRequ deptDeletedRequ);

    List<Dept> getChildrenDeptsById(Long deptId);
}
