package com.singleton.server.system.controller;

import com.singleton.server.common.component.ApiComponent;
import com.singleton.server.common.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "API")
@RestController
@RequestMapping("/system/api")
public class ApiController {
    @Autowired
    private ApiComponent apiComponent;

    @GetMapping("/tree")
    @ApiOperation("树型结构")
    public Result<List<ApiComponent.ApiMateInfo>> tree() {
        return Result.ok(apiComponent.buildApis());
    }
}
