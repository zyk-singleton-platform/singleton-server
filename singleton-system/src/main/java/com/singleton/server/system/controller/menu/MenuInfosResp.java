package com.singleton.server.system.controller.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel("菜单信息响应")
public class MenuInfosResp implements Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("平台CODE")
    private String platformCode;

    @ApiModelProperty("位置")
    private Long treeIndex;

    @ApiModelProperty("父位置")
    private Long pIndex;

    @ApiModelProperty("路由Key")
    private String routeKey;

    @ApiModelProperty("菜单名称")
    private String title;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("元数据")
    private String meta;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("已删除 1-已删除 0-未删除")
    private Boolean deleted;

    @ApiModelProperty("子菜单列表")
    private List<MenuInfosResp> childrens;

    @ApiModelProperty("API列表")
    private List<String> apiUrls;
}
