package com.singleton.server.system.controller.user;


import com.singleton.server.common.domain.PageFilter;
import com.singleton.server.common.domain.PageResult;
import com.singleton.server.common.result.Result;
import com.singleton.server.system.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统-用户表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-10-08
 */
@Api(tags = "用户-API")
@RestController
@RequestMapping("/system/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("新增")
    @PostMapping("/new")
    public Result<Boolean> newUser(@RequestBody @Validated UserNewRequ userNewRequ) {
        Boolean aBoolean = userService.addUser(userNewRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("分页过滤")
    @PostMapping("/pageFilter")
    public Result<PageResult<UserInfosResp>> pageFilter(@RequestBody PageFilter<UserPageFilterRequ> pageFilter) {
        PageResult<UserInfosResp> page = userService.pageFilter(pageFilter);
        return Result.ok(page);
    }

    @ApiOperation("更新")
    @PostMapping("/edited")
    public Result<Boolean> edited(@RequestBody @Validated UserEditedRequ userEditedRequ){
        Boolean aBoolean = userService.edited(userEditedRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("删除")
    @PostMapping("/deleted")
    public Result<Boolean> deleted(@RequestBody @Validated UserDeletedRequ userDeletedRequ){
        Boolean aBoolean = userService.deleted(userDeletedRequ);
        return Result.ok(aBoolean);
    }

}
