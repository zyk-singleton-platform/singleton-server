package com.singleton.server.system.mapper;

import com.singleton.server.system.entity.RoleFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统-角色功能表 Mapper 接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Repository
public interface RoleFunctionMapper extends BaseMapper<RoleFunction> {

}
