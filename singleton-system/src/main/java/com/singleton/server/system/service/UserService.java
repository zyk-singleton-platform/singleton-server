package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.common.domain.PageFilter;
import com.singleton.server.common.domain.PageResult;
import com.singleton.server.system.controller.user.*;
import com.singleton.server.system.dto.UserDTO;
import com.singleton.server.system.entity.User;

/**
 * <p>
 * 系统-用户表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
public interface UserService extends IService<User> {

    UserDTO getUserRoleByAccount(String account);

    Boolean addUser(UserNewRequ userNewRequ);

    PageResult<UserInfosResp> pageFilter(PageFilter<UserPageFilterRequ> pageFilter);

    Boolean edited(UserEditedRequ userEditedRequ);

    Boolean deleted(UserDeletedRequ userDeletedRequ);
}
