package com.singleton.server.system.service;

import com.singleton.server.system.entity.Platform;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统-平台表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-11-22
 */
public interface PlatformService extends IService<Platform> {

}
