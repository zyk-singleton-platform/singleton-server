package com.singleton.server.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.common.domain.PageFilter;
import com.singleton.server.common.domain.PageResult;
import com.singleton.server.system.controller.role.*;
import com.singleton.server.system.dto.RoleDTO;
import com.singleton.server.system.entity.Role;

import java.util.List;

/**
 * <p>
 * 系统-角色表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
public interface RoleService extends IService<Role> {

    List<RoleDTO> getRolesByUserId(Long id);

    List<RoleDTO> getRolesByIds(List<Long> roleIds);

    Boolean addRole(RoleNewRequ roleNewRequ);

    PageResult<RoleInfosResp> pageFilter(PageFilter<RolePageFilterRequ> pageFilter);

    Boolean edited(RoleEditedRequ roleEditedRequ);

    Boolean deleted(RoleDeletedRequ roleDeletedRequ);

    List<RoleInfosResp> listAll();

    List<RoleDTO> getRolesByRoleCode(String roleCode);
}
