package com.singleton.server.system.controller.function;

import com.singleton.server.system.controller.functionapi.FunctionApiNewRequ;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("功能修改请求")
public class FunctionEditedRequ implements Serializable{

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("主键")
    @NotNull(message = "ID不能为空!")
    private Long id;

    @ApiModelProperty("功能名称")
    @NotNull(message = "功能名称不能为空!")
    private String name;

    @ApiModelProperty("所属菜单ID")
    @NotNull(message = "所属菜单ID不能为空!")
    private Long menuId;

    @Valid
    @ApiModelProperty("功能Api列表")
    private List<FunctionApiNewRequ> functionApiNewRequs;
}
