package com.singleton.server.system.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统-角色
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色代码
     */
    private String code;

    /**
     * 角色类型（0：业务角色，1: 管理角色）
     */
    private Integer type;

    /**
     * 是否可用
     */
    private Integer enable;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 已删除
     */
    private Integer deleted;
}
