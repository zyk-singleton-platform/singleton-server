package com.singleton.server.system.controller.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户分页过滤请求
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@ApiModel("用户分页过滤请求信息")
public class UserNewRequ  implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("部门编号")
    @NotNull(message = "部门编号不能为空")
    private Long deptId;

    @ApiModelProperty("角色")
    @NotEmpty(message = "角色编号不能为空")
    private List<Long> roleIds;

    @ApiModelProperty("用户名")
    @NotEmpty(message = "用户名不能为空")
    private String name;

    @ApiModelProperty("账号")
    @NotEmpty(message = "账号不能为空")
    private String account;

    @ApiModelProperty("密码")
    @NotEmpty(message = "密码不能为空")
    private String password;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

}
