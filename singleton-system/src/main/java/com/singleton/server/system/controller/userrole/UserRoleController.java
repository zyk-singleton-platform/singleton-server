package com.singleton.server.system.controller.userrole;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统-用户角色关联表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-10-08
 */
@RestController
@RequestMapping("/system/user-role")
public class UserRoleController {

}
