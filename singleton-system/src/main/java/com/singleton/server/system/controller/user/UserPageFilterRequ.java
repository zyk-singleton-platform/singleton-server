package com.singleton.server.system.controller.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * 用户分页过滤请求
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@ApiModel("用户分页过滤请求信息")
public class UserPageFilterRequ  implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("部门编号")
    private Long deptId;

    @ApiModelProperty("用户名")
    private String name;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

}
