package com.singleton.server.system.mapper;

import com.singleton.server.system.entity.Platform;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统-平台表 Mapper 接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-11-22
 */
@Repository
public interface PlatformMapper extends BaseMapper<Platform> {

}
