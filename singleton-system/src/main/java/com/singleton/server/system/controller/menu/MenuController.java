package com.singleton.server.system.controller.menu;


import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.common.result.Result;
import com.singleton.server.system.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统-菜单表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-10-08
 */
@Api(tags = "菜单-API")
@RestController
@RequestMapping("/system/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @ApiOperation("树")
    @GetMapping("/tree")
    public Result<List<MenuInfosResp>> tree(@RequestParam("platformCode") String platformCode){
        List<MenuInfosResp> list = menuService.tree(platformCode);
        return Result.ok(list);
    }
    @ApiOperation("同步")
    @PostMapping("/sync")
    public Result<Boolean> syncMenuInfos(@RequestBody @Validated List<MenuSyncRequ> menuSyncRequs, @RequestParam("platformCode") String platformCode){
        Boolean aBoolean = menuService.syncMenuInfos(menuSyncRequs, platformCode);
        return Result.ok(aBoolean);
    }
    @ApiOperation("列表")
    @GetMapping("/list")
    public Result<List<MenuInfosResp>> list(@RequestParam("platformCode") String platformCode){
        List<MenuInfosResp> list = menuService.listAll(platformCode);
        return Result.ok(list);
    }

    @ApiOperation(value = "菜单")
    @RequestMapping(value = "/current", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<MenuInfosResp>> getMenus(@RequestParam("platformCode") String platformCode){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        TokenContext tokenContext = (TokenContext)authentication.getPrincipal();
        List<MenuInfosResp> list = menuService.treeByUserId(tokenContext.getUserId(), platformCode);
        return Result.ok(list);
    }

}
