package com.singleton.server.system.controller.functionapi;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统-功能api关系表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-10-08
 */
@RestController
@RequestMapping("/system/function-api")
public class FunctionApiController {

}
