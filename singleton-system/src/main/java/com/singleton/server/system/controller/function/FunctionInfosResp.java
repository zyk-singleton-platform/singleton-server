package com.singleton.server.system.controller.function;

import com.singleton.server.system.entity.FunctionApi;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel("功能信息响应")
public class FunctionInfosResp implements Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("平台CODE")
    private String platformCode;

    @ApiModelProperty("功能名称")
    private String name;

    @ApiModelProperty("所属菜单ID")
    private Long menuId;

    @ApiModelProperty("所属菜单名称")
    private String menuName;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("已删除 1-已删除 0-未删除")
    private Boolean deleted;

    @ApiModelProperty("api列表")
    private List<FunctionApi> functionApis;

}
