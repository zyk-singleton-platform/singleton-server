package com.singleton.server.common.component;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 保存到缓存中
 */
@Slf4j
@Component
public class ApiComponent {

    @Autowired
    private RequestMappingHandlerMapping mapping;

    public List<ApiMateInfo> buildApis() {
        List<ApiMateInfo> apis = new ArrayList<>();
        HashMap<String, List<ApiMateInfo>> handlerMapping = new HashMap<>();
        HashMap<String, String> mapClassName2Des = new HashMap<>();
        // 获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        // 遍历服务接口信息，筛选符合条件的数据
        map.forEach((mappingInfo, handlerMethod) -> {
            // 类
            Class<?> controllerClass = handlerMethod.getBeanType();
            // 包路径
            String classPackage = controllerClass.getName();
            if (verifyClassPackageHasProperties(classPackage, "com.singleton.**.controller.**")) {
                // 方法
                Method method = handlerMethod.getMethod();
                ApiOperation apiOperation = AnnotatedElementUtils.findMergedAnnotation(method, ApiOperation.class);
                Api api = AnnotatedElementUtils.findMergedAnnotation(controllerClass, Api.class);
                if(apiOperation == null){
                    return;
                }
                // 获取方法路径
                String[] methodPaths = mappingInfo.getPatternsCondition().getPatterns().toArray(new String[]{});
                // 生成数据
                ApiMateInfo apiMateInfo = new ApiMateInfo(classPackage + "#" + method.getName(), methodPaths[0], apiOperation.value(), null);
                List<ApiMateInfo> apiMateInfos = handlerMapping.get(classPackage);
                if(null == apiMateInfos){
                    apiMateInfos = new ArrayList<>();
                    handlerMapping.put(classPackage, apiMateInfos);
                }
                apiMateInfos.add(apiMateInfo);
                String pdes = api == null ? classPackage : Optional.ofNullable(api.value()).filter(StringUtils::isNotEmpty).orElse(ArrayUtils.get(api.tags(), 0));
                mapClassName2Des.put(classPackage, pdes);
            }
        });
        for (Map.Entry<String, String> stringStringEntry : mapClassName2Des.entrySet()) {
            String key = stringStringEntry.getKey();
            String value = stringStringEntry.getValue();

            List<ApiMateInfo> apiMateInfos = handlerMapping.get(key);
            if(apiMateInfos == null){
                apiMateInfos = Collections.emptyList();
            }
            apiMateInfos.forEach(item -> {
                item.setDes("[" + value + "]--" + item.getDes());
            });
            apis.add(new ApiMateInfo(key, null, value, apiMateInfos));
        }
        return apis;
    }

    /**
     * 验证包路径
     *
     * @param classPackage 需要验证的包路径
     * @param scanPackages 验证条件的包路径，可以传入多个
     * @return 验证结果，只要有一个条件符合，条件就会成立并返回True
     */
    private boolean verifyClassPackageHasProperties(String classPackage, String... scanPackages) {
        for (String scanPackage : scanPackages) {
            if (Pattern.matches(buildRegexPackage(scanPackage), classPackage)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 转换验证条件，使其支持正则验证
     *
     * @param scanPackage 验证条件包路径
     * @return 验证条件正则
     */
    private String buildRegexPackage(String scanPackage) {
        return scanPackage.replace("**", "[\\w.]*") + ".[\\w]*";
    }

    @Data
    public static class ApiMateInfo{
        public ApiMateInfo() {
        }

        public ApiMateInfo(String key, String path, String des, List<ApiMateInfo> childrens) {
            this.key = key;
            this.path = path;
            this.des = des;
            this.childrens = childrens;
        }

        private String key;
        private String path;
        private String des;
        private List<ApiMateInfo> childrens;
    }
}
