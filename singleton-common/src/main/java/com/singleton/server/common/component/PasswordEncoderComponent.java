package com.singleton.server.common.component;

import com.singleton.server.common.utils.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * 重写BCryptPasswordEncoder类的加密方法和密码对比方法
 */
@Slf4j
@Component
public class PasswordEncoderComponent extends BCryptPasswordEncoder {
    private static final String ENCODER_SPLIT = "$#&";

    public String encode(String pwd, String random) {
        if(StringUtils.isEmpty(pwd)) {
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
        String encodeStr;
        if(StringUtils.isNotEmpty(random)) {
            encodeStr = MD5Util.computeMD5(pwd + ENCODER_SPLIT + random);
        } else {
            encodeStr = MD5Util.computeMD5(pwd);
        }
        return encodeStr;
    }

    public boolean matches(String pwd, String encodedPassword, String random) {
        if(StringUtils.isEmpty(pwd)) {
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
        if(StringUtils.isEmpty(encodedPassword)) {
            log.warn("Empty encoded password");
            return false;
        }
        if(StringUtils.isNotEmpty(random)){
            encodedPassword =encode(encodedPassword, random);
        }
        return StringUtils.equals(pwd, encodedPassword);
    }
}