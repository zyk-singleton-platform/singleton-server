package com.singleton.server.common.domain;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import io.jsonwebtoken.Claims;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class TokenContext implements UserDetails {
    private List<String> roles = new ArrayList<>();
    private String username;
    private Long userId;
    private Boolean enabled;
    private String account;
    private String phone;
    private String email;
    private Boolean refresh;
    private Collection<? extends GrantedAuthority> authorities;

    public TokenContext() { }

    public TokenContext(List<String> roles, String username, Long userId, Boolean enabled, String account, String phone, String email) {
        this.roles = roles;
        this.username = username;
        this.userId = userId;
        this.enabled = enabled;
        this.account = account;
        this.phone = phone;
        this.email = email;
        this.authorities = roles.stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
    }

    public TokenContext(Claims claims) {
        JSONObject jsonObject = new JSONObject(claims);
        this.roles = jsonObject.getObject("roles", new TypeReference<List<String>>(){});
        this.username = jsonObject.getString("username");
        this.userId = jsonObject.getLong("userId");
        this.enabled = jsonObject.getBoolean("enabled");
        this.account = jsonObject.getString("account");
        this.phone = jsonObject.getString("phone");
        this.email = jsonObject.getString("email");
        this.refresh = jsonObject.getBoolean("refresh");
        this.authorities = roles.stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
    }

    public HashMap<String, Object> toTokenMap(){
        return toMap(Boolean.FALSE);
    }

    public HashMap<String, Object> toRefreshTokenMap(){
        return toMap(Boolean.TRUE);
    }

    public HashMap<String, Object> toMap(Boolean isRefresh){
        HashMap<String, Object> tokenMap = new HashMap<>(5);
        tokenMap.put("roles", roles);
        tokenMap.put("username", username);
        tokenMap.put("userId" , userId);
        tokenMap.put("enabled", enabled);
        tokenMap.put("account" , account);
        tokenMap.put("phone", phone);
        tokenMap.put("email", email);
        tokenMap.put("refresh", isRefresh);
        return tokenMap;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    /**
     * 账户是否过期
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 是否禁用
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return  true;
    }

    /**
     * 密码是否过期
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否启用
     * @return
     */
    @Override
    public boolean isEnabled() {
        return Boolean.TRUE;
    }
}