package com.singleton.server.common.basic;


import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

public abstract class BasicException extends RuntimeException implements BasicErrorCode{
    private static final long serialVersionUID = -5365630128856068163L;
    private int code;
    private String message;
    private String stack;

    public BasicException(BasicErrorCode basicErrorCode) {
        super(basicErrorCode.getMessage());
        this.code = basicErrorCode.getCode();
        this.message = basicErrorCode.getMessage();
    }

    public BasicException(BasicErrorCode basicErrorCode, String message) {
        super(basicErrorCode.getMessage());
        this.code = basicErrorCode.getCode();
        this.message = message;
    }

    public BasicException(BasicErrorCode basicErrorCode, Throwable cause) {
        super(basicErrorCode.getMessage(), cause);
        this.code = basicErrorCode.getCode();
        this.message = basicErrorCode.getMessage();
        this.stack = parseStackTrace(cause);
    }

    private String parseStackTrace(Throwable error) {
        StringWriter stackTrace = new StringWriter();
        error.printStackTrace(new PrintWriter(stackTrace));
        stackTrace.flush();
        return stackTrace.toString();
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getStack() {
        return stack;
    }
}
