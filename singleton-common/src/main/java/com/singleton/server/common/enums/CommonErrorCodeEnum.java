package com.singleton.server.common.enums;

import com.singleton.server.common.basic.BasicErrorCode;

public enum CommonErrorCodeEnum implements BasicErrorCode {
    SERVER_ERROR(101,"服务出错"),
    BASE_PARAM_ERROR(102,"参数校验不通过"),
    BASE_BAD_REQUEST_ERROR(103,"无效的请求"),
    ;

    private final int code;

    private final String desc;

    private final int moduleCode = ModuleEnum.COMMON.getCode();

    CommonErrorCodeEnum(int code, String desc) {
        this.code = ERROR_CODE_LEVEL * moduleCode + code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return desc;
    }

}
