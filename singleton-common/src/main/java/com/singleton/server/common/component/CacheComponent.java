package com.singleton.server.common.component;

import com.singleton.server.common.enums.CachesEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

/**
 * 保存到缓存中
 */
@Slf4j
@Component
public class CacheComponent {
    @Autowired
    CacheManager cacheManager;

    public boolean saveCache(CachesEnum cachesEnum, String key, Object value) {
        if (null == cachesEnum || key == null) {
            return false;
        }
        cacheManager.getCache(cachesEnum.name()).put(key, value);
        return true;
    }

    public String getCacheString(CachesEnum cachesEnum, String key) {
        if (null == cachesEnum || key == null) {
            return null;
        }
        ValueWrapper valueWrapper = cacheManager.getCache(cachesEnum.name()).get(key);
        if (valueWrapper != null) {
            return String.valueOf(valueWrapper.get());
        } else {
            return null;
        }
    }
    public Object getCache(CachesEnum cachesEnum, String key) {
        if (null == cachesEnum || key == null) {
            return null;
        }
        ValueWrapper valueWrapper = cacheManager.getCache(cachesEnum.name()).get(key);
        if (valueWrapper != null) {
            return valueWrapper.get();
        } else {
            return null;
        }
    }

    public Cache getCache(CachesEnum cachesEnum) {
        if (null == cachesEnum) {
            return null;
        }
        return cacheManager.getCache(cachesEnum.name());
    }
    public void remove(CachesEnum cachesEnum, String key) {
        if (null == cachesEnum || key == null) {
            return;
        }
        cacheManager.getCache(cachesEnum.name()).evict(key);
    }
}
