package com.singleton.server.common.basic;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public interface BasicEnum {
    String desc = null;
    String value = null;

    /**
     * 获取枚举类的值
     *
     * @return Object
     */
    default String getValue(){
        return value;
    }

    /**
     * 获取枚举类的说明
     *
     * @return String
     */
    default String getDesc() {
        return desc;
    }

    /**
     * 比较参数是否与枚举类的value相同
     *
     * @param value
     * @return boolean
     */
    default boolean equalsValue(String value) {
        return Objects.equals(getValue(), value);
    }

    /**
     * 比较枚举类是否相同
     *
     * @param basicEnum
     * @return boolean
     */
    default boolean equals(BasicEnum basicEnum) {
        return Objects.equals(getValue(), basicEnum.getValue()) && Objects.equals(getDesc(), basicEnum.getDesc());
    }

    /**
     * 返回枚举类的说明
     *
     * @param clazz 枚举类类对象
     * @return
     */
    static List<JSONObject> getInfo(Class<? extends BasicEnum> clazz) {
        BasicEnum[] enums = clazz.getEnumConstants();
        List<JSONObject> list = new ArrayList<>(enums.length);
        for (BasicEnum e : enums) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", e.getValue());
            jsonObject.put("desc", e.getDesc());
            list.add(jsonObject);
        }
        return list;
    }
}
