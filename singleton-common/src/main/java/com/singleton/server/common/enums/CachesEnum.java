package com.singleton.server.common.enums;

public enum CachesEnum {
    /**
     * 使用默认值
     */
    DefaultCache,
    SmsCaptchaCache(60 * 3, "短信验证码"),
    GraphCaptchaCache(60 * 5, 100000, "图形验证码"),
    CaptchaTimesCache(60 * 5, 100000, "验证码最试次数"),
    TokenCache(0, 100000, "token缓存"),
    RefreshAccessTokenCache(0, 100000, "Refresh-token,token 关系缓存"),
    RefreshTokenCache(0, 100000, "Refresh-token缓存"),
    ;

    CachesEnum() {
    }

    CachesEnum(int ttl, String desc) {
        this.ttl = ttl;
        this.desc = desc;
    }

    CachesEnum(int ttl, int maxSize, String desc) {
        this.ttl = ttl;
        this.maxSize = maxSize;
        this.desc = desc;
    }

    private int maxSize = 100000;
    private int ttl = 60 * 5;
    private String desc = "缓存";

    public int getMaxSize() {
        return maxSize;
    }

    public int getTtl() {
        return ttl;
    }
}
