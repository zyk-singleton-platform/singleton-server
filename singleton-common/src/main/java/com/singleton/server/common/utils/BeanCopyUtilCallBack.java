package com.singleton.server.common.utils;

@FunctionalInterface
public interface BeanCopyUtilCallBack<S, T> {
    /**
     * 回调
     **/
    void callBack(S t, T s);
}
