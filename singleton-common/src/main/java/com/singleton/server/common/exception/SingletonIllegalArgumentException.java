package com.singleton.server.common.exception;

import com.singleton.server.common.basic.BasicErrorCode;
import com.singleton.server.common.basic.BasicException;
import com.singleton.server.common.enums.CommonErrorCodeEnum;

public class SingletonIllegalArgumentException extends BasicException {
    public SingletonIllegalArgumentException(BasicErrorCode basicErrorCode) {
        super(basicErrorCode);
    }

    public SingletonIllegalArgumentException(BasicErrorCode basicErrorCode, Throwable cause) {
        super(basicErrorCode, cause);
    }

    public SingletonIllegalArgumentException(String message) {
        super(CommonErrorCodeEnum.BASE_PARAM_ERROR, message);
    }
}
