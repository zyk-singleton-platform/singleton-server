package com.singleton.server.common.enums;

public enum ModuleEnum{
    COMMON(100, "公共模块"),
    AUTH(110, "授权认证"),
    SYSTEM(120, "系统模块");

    private final int code;
    private final String desc;

    ModuleEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return this.code;
    }

    public String getDesc() {
        return this.desc;
    }
}
