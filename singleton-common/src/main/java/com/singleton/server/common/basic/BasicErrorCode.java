package com.singleton.server.common.basic;

public interface BasicErrorCode {
    int ERROR_CODE_LEVEL = 1000;

    int getCode();

    String getMessage();

    default String getStack(){
        return null;
    }
}
