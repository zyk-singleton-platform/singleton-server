package com.singleton.server.common.domain;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel("分页过滤请求信息")
public class PageFilter<T> implements Serializable {
    private static final long serialVersionUID = 8545996863226528798L;

    @ApiModelProperty("过滤信息")
    private T filter;

    @ApiModelProperty("总数")
    protected long total = 0;

    @ApiModelProperty("每页显示条数，默认 15")
    protected long size = 15;

    @ApiModelProperty("当前页")
    protected long current = 1;

    @ApiModelProperty(value = "排序字段信息")
    protected List<OrderItem> orders = new ArrayList<>();

    public PageFilter() {
    }

    /**
     * 分页构造函数
     *
     * @param current 当前页
     * @param size    每页显示条数
     */
    public PageFilter(long current, long size) {
        this(current, size, 0);
    }

    public PageFilter(long current, long size, long total) {
        if (current > 1) {
            this.current = current;
        }
        this.size = size;
        this.total = total;
    }

    public Page toPage(){
        Page page = new Page(this.current, this.size, this.total, Boolean.TRUE);
        page.addOrder(orders);
        return page;
    }

    @Data
    @ApiModel("排序规则")
    public static class OrderItem extends com.baomidou.mybatisplus.core.metadata.OrderItem {
        @ApiModelProperty("需要进行排序的字段")
        private String column;
        @ApiModelProperty("是否正序排列，默认 true")
        private boolean asc = true;
    }
}
