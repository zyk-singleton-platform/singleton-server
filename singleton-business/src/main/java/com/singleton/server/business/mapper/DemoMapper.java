package com.singleton.server.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.singleton.server.business.entity.Demo;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统-权限功能表 Mapper 接口
 * </p>
 *
 * @author Shuai.Zhang
 * @since 2021-08-13
 */
@Repository
public interface DemoMapper extends BaseMapper<Demo> {

}
