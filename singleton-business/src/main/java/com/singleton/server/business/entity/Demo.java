package com.singleton.server.business.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@EqualsAndHashCode(callSuper = false)
@TableName("business_demo")
public class Demo extends Model<Demo> {

    private static final long serialVersionUID = 1L;

    /**
     * 功能权限表主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
