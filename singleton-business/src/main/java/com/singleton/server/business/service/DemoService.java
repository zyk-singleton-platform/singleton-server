package com.singleton.server.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.singleton.server.business.entity.Demo;

/**
 * <p>
 * 系统-权限功能表 服务类
 * </p>
 *
 * @author Shuai.Zhang
 * @since 2021-08-13
 */
public interface DemoService extends IService<Demo> {

}
