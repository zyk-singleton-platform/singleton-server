package com.singleton.server.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.singleton.server.business.entity.Demo;
import com.singleton.server.business.mapper.DemoMapper;
import com.singleton.server.business.service.DemoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统-权限功能表 服务实现类
 * </p>
 *
 * @author Shuai.Zhang
 * @since 2021-08-13
 */
@Service
public class DemoServiceImpl extends ServiceImpl<DemoMapper, Demo> implements DemoService {

}
