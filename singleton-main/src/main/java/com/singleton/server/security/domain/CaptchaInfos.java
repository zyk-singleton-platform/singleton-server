package com.singleton.server.security.domain;

import lombok.Data;

@Data
public class CaptchaInfos {
    private Integer ttl;
    private String graphId;
    private String graphUrl;
    private String base64EncodedGraph;
    private String captcha;
}
