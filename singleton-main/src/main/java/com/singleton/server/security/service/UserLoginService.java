package com.singleton.server.security.service;

import com.singleton.server.config.security.properties.AuthProperties;
import com.singleton.server.config.security.store.ITokenStore;
import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.security.domain.TokenInfos;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.security.exception.AuthException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserLoginService {
    @Autowired
    private ITokenStore tokenStore;

    @Autowired
    private AuthProperties authProperties;

    /**
     * 登录功能
     *
     * @return
     */
    public TokenInfos buildTokenInfos() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        TokenContext tokenContext = (TokenContext)authentication.getPrincipal();
        TokenInfos tokenInfos = new TokenInfos();
        tokenInfos.add(tokenContext);
        return buildTokenInfos(tokenInfos);
    }

    /**
     * 刷新token
     * @param oldToken
     * @return
     */
    public TokenInfos refreshToken(String oldToken) {
        TokenContext refreshTokenContext = tokenStore.getRefreshTokenContext(oldToken);
        if(refreshTokenContext == null){
            throw new AuthException(AuthErrorCodeEnum.TOKEN_PARSE_ERROR);
        }
        if(refreshTokenContext.getRefresh()){
            TokenInfos tokenInfos = new TokenInfos();
            tokenInfos.add(refreshTokenContext);
            return buildTokenInfos(tokenInfos);
        }
        throw new AuthException(AuthErrorCodeEnum.TOKEN_PARSE_ERROR);
    }

    private TokenInfos buildTokenInfos(TokenInfos tokenInfos){
        TokenContext tokenContext = tokenInfos.buildTokenContext();
        ITokenStore.Token token = tokenStore.createdToken(tokenContext);
        tokenInfos.setAccessToken(token.getAccessToken());
        tokenInfos.setRefreshToken(token.getRefreshToken());
        tokenInfos.setTokenPrefix(authProperties.getTokenPrefix());
        tokenInfos.setExpiration(authProperties.getExpiration());
        tokenInfos.setRefreshExpiration(authProperties.getRefreshExpiration());
        return tokenInfos;
    }

    public Boolean logout(String account) {
        return tokenStore.remove(account);
    }
}
