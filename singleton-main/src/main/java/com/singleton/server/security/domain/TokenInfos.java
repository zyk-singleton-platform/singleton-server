package com.singleton.server.security.domain;

import com.singleton.server.common.domain.TokenContext;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class TokenInfos {
    private String accessToken;
    private String refreshToken;
    private String tokenPrefix;
    private Long expiration;
    private Long refreshExpiration;
    private List<String> roles;
    private String username;
    private Long userId;
    private Boolean enabled;
    private String account;
    private String phone;
    private String email;
    private String remark;
    private LocalDateTime createTime;
    public void add(LocalUsersssDetails userDetails){
        roles = userDetails.getRoleCodes();
        username = userDetails.getUsername();
        userId = userDetails.getId();
        enabled = userDetails.isEnabled();
        account = userDetails.getAccount();
        phone = userDetails.getPhone();
        email = userDetails.getEmail();
        remark = userDetails.getRemark();
    }
    public void add(TokenContext tokenContext){
        roles = tokenContext.getRoles();
        username = tokenContext.getUsername();
        userId = tokenContext.getUserId();
        enabled = tokenContext.isEnabled();
        account = tokenContext.getAccount();
        phone = tokenContext.getPhone();
        email = tokenContext.getEmail();
    }

    public TokenContext buildTokenContext() {
        return new TokenContext(roles, username, userId, enabled, account, phone, email);
    }
}
