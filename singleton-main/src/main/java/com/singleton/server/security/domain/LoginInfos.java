package com.singleton.server.security.domain;

import io.swagger.annotations.*;
import lombok.Data;

@Data
@ApiModel("登录请求体")
public class LoginInfos {
    @ApiModelProperty("账号")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("验证码")
    private String validateCode;
    @ApiModelProperty("验证ID")
    private String graphId;
    @ApiModelProperty("刷新token")
    private String refreshToken;
}
