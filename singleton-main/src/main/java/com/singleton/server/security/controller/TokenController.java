package com.singleton.server.security.controller;

import com.singleton.server.common.result.Result;
import com.singleton.server.config.security.properties.AuthProperties;
import com.singleton.server.security.domain.LoginInfos;
import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.security.domain.TokenInfos;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.security.exception.AuthException;
import com.singleton.server.security.service.UserLoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(tags = "token管理")
@RestController
@RequestMapping("/token")
public class TokenController {

    @Autowired
    UserLoginService userService;

    @Autowired
    AuthProperties userAuthProperties;

    @ApiOperation(value = "登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Result<TokenInfos> login(LoginInfos loginInfos){
        TokenInfos tokenInfos = userService.buildTokenInfos();
        return Result.ok(tokenInfos);
    }

    @ApiOperation(value = "信息")
    @RequestMapping(value = "/infos", method = RequestMethod.GET)
    @ResponseBody
    public Result<TokenContext> getInfo(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        TokenContext tokenContext = (TokenContext)authentication.getPrincipal();
        return Result.ok(tokenContext);
    }

    @ApiOperation(value = "登出")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> logout(HttpServletRequest request, HttpServletResponse response){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        TokenContext tokenContext = (TokenContext)authentication.getPrincipal();
        String account = tokenContext.getAccount();
        boolean success = userService.logout(account);
        if (success && authentication!=null){
            new SecurityContextLogoutHandler().logout(request,response,authentication);
        }
        return Result.ok("成功登出");
    }

    @ApiOperation(value = "刷新token")
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    @ResponseBody
    public Result<TokenInfos> refreshToken(LoginInfos loginInfos) {
        if (loginInfos==null || StringUtils.isEmpty(loginInfos.getRefreshToken())){
            throw new AuthException(AuthErrorCodeEnum.TOKEN_IS_NULL_ERROR);
        }
        TokenInfos tokenInfos = userService.refreshToken(loginInfos.getRefreshToken());
        if (tokenInfos == null) {
            throw new AuthException(AuthErrorCodeEnum.REFRESH_TOKEN_ERROR);
        }
        return Result.ok(tokenInfos);
    }
}