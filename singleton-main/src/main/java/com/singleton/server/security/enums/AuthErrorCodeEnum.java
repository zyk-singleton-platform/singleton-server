package com.singleton.server.security.enums;

import com.singleton.server.common.basic.BasicErrorCode;
import com.singleton.server.common.enums.ModuleEnum;

public enum AuthErrorCodeEnum implements BasicErrorCode {
    LOGOUT_ERROR(100,"错误的请求"),
    ACCOUNT_NOT_EXIST(101,"账号不存在"),
    REQUIRED_LOGIN_ERROR(102,"请先登录"),
    LOGIN_UNMATCH_ERROR(103,"未找到该用户信息"),
    PERMISSION_DENY_ERROR(104,"抱歉，您没有访问权限"),
    TOKEN_IS_NULL_ERROR(105,"抱歉，您没有访问权限"),
    REFRESH_TOKEN_ERROR(106,"抱歉，刷新TOKEN失败"),
    TOKEN_PARSE_ERROR(107,"抱歉，TOKEN解析失败"),
    TOKEN_EXPIRED_ERROR(108,"抱歉，TOKEN过期"),
    CAPTCHA_CODE_ERROR(109,"抱歉，验证码错误"),
    CREATED_REFRESH_TOKEN_ERROR(110,"创建 refresh_token 失败"),
    CREATED_ACCESS_TOKEN_ERROR(111,"创建 access_token 失败"),
    ROLE_API_AUTH_ERROR(112,"未授权"),
    CAPTCHA_CODE_INVALID(113,"抱歉，验证码失效"),
    CAPTCHA_CODE_OR_PASSWORD_ERROR(114,"验证码或密码不正确"),
    ;

    private final int code;

    private final String desc;

    private final int moduleCode = ModuleEnum.AUTH.getCode();

    AuthErrorCodeEnum(int code, String desc) {
        this.code = ERROR_CODE_LEVEL * moduleCode + code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return desc;
    }
}
