package com.singleton.server.security.domain;

import lombok.Data;

@Data
public class SmsInfos {
    private Integer ttl;
    private String smsId;
}
