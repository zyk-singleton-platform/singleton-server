package com.singleton.server.security.domain;

import com.singleton.server.security.constants.UserConstants;
import com.singleton.server.system.dto.RoleDTO;
import com.singleton.server.system.dto.UserDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class LocalUsersssDetails extends UserDTO implements UserDetails {
    private List<String> roleCodes;
    private Collection<? extends GrantedAuthority> authorities;
    public LocalUsersssDetails(UserDTO userDTO){
        this.setId(userDTO.getId());
        this.setAccount(userDTO.getAccount());
        this.setPassword(userDTO.getPassword());
        this.setCreateTime(userDTO.getCreateTime());
        this.setDeptId(userDTO.getDeptId());
        this.setEmail(userDTO.getEmail());
        this.setName(userDTO.getName());
        this.setPhone(userDTO.getPhone());
        this.roleCodes = userDTO.getRoles().stream().map(RoleDTO::getCode).filter(StringUtils::isNotEmpty).collect(Collectors.toList());
        List<SimpleGrantedAuthority> authorities = roleCodes.stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
        this.setAuthorities(authorities);
    }

    public List<String> getRoleCodes() {
        return roleCodes;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    /**
     * 添加用户拥有的权限和角色
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getUsername() {
        return this.getAccount();
    }

    /**
     * 账户是否过期
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 是否禁用
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return  true;
    }

    /**
     * 密码是否过期
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否启用
     * @return
     */
    @Override
    public boolean isEnabled() {
         return UserConstants.USER_STATUS_NORMAL.equals(this.getEnable());
    }
}
