package com.singleton.server.security.service;

import com.singleton.server.security.domain.LocalUsersssDetails;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.security.exception.AuthException;
import com.singleton.server.system.dto.UserDTO;
import com.singleton.server.system.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 自己实现UserDetailService,用与SpringSecurity获取用户信息
 */

@Slf4j
@Service
public class LocalUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    /**
     * 获取用户信息,然后交给spring去校验权限
     * @param account
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        //获取用户信息
        UserDTO userDTO = userService.getUserRoleByAccount(account);
        if(userDTO == null){
            throw new AuthException(AuthErrorCodeEnum.ACCOUNT_NOT_EXIST);
        }
        LocalUsersssDetails customerUserDetails = new LocalUsersssDetails(userDTO);
        return customerUserDetails;
    }
}
