package com.singleton.server.security.service;

import com.singleton.server.common.component.CacheComponent;
import com.singleton.server.common.enums.CachesEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.stereotype.Service;

/**
 * 保存到缓存中
 */
@Service
public class CaptchaService{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${captcha.max.times:5}")
    private int captchaMaxTimes;

    @Autowired
    CacheComponent cacheComponent;

    public boolean saveCaptcha(CachesEnum cachesEnum, String key, Object value) {
        cacheComponent.getCache(cachesEnum).put(key, value);
        return true;
    }

    public String getCaptcha(CachesEnum cachesEnum, String key) {
        if (!checkCaptchaTimes(cachesEnum, key)) {
            return null;
        }
        ValueWrapper valueWrapper = cacheComponent.getCache(cachesEnum).get(key);
        if (valueWrapper != null) {
            return String.valueOf(valueWrapper.get());
        } else {
            return null;
        }
    }

    public void removeCaptcha(CachesEnum cachesEnum, String key) {
        cacheComponent.getCache(cachesEnum).evict(key);
    }


    public boolean checkCaptchaTimes(CachesEnum cachesEnum, String key) {
        ValueWrapper valueWrapper = cacheComponent.getCache(CachesEnum.CaptchaTimesCache).get(key);
        if (valueWrapper != null) {
            int times = Integer.valueOf(String.valueOf(valueWrapper.get()));
            if (times < captchaMaxTimes) {
                cacheComponent.getCache(CachesEnum.CaptchaTimesCache).put(key, times + 1);
                return true;
            } else {
                log.debug("验证码达到最大尝试次数：" + cachesEnum + "_" + key);
                removeCaptcha(cachesEnum, key);
                removeCaptcha(CachesEnum.CaptchaTimesCache, key);
                return false;
            }
        } else {
            cacheComponent.getCache(CachesEnum.CaptchaTimesCache).put(key, 1);
            return true;
        }
    }

}
