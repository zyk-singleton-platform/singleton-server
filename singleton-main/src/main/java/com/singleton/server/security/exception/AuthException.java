package com.singleton.server.security.exception;

import com.singleton.server.common.basic.BasicErrorCode;
import com.singleton.server.common.basic.BasicException;
import com.singleton.server.common.enums.ModuleEnum;

public class AuthException extends BasicException {
    public AuthException(BasicErrorCode basicErrorCode) {
        super(basicErrorCode);
    }

    public AuthException(BasicErrorCode basicErrorCode, Throwable cause) {
        super(basicErrorCode, cause);
    }

}
