package com.singleton.server.security.constants;

public interface UserConstants {

    Integer USER_STATUS_NORMAL = 0;

    String USER_ID = "userId";
}