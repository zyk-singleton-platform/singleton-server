package com.singleton.server.security.controller;

import com.singleton.server.common.captcha.core.VerificationCodeUtil;
import com.singleton.server.common.enums.CachesEnum;
import com.singleton.server.common.result.Result;
import com.singleton.server.security.domain.CaptchaInfos;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.security.service.CaptchaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Api(tags = "验证-API")
@RestController
@RequestMapping("/captcha")
public class CaptchaController {

    @Value("${spring.profiles.active:prod}")
    private String active;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private CaptchaService captchaService;

    @Autowired
    public CaptchaController(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    /**
     * 短信证码
     *
     * @param phone   手机号
     * @param graphId 图形验证码id
     */
    @ResponseBody
    @GetMapping("/sms")
    @ApiOperation(value = "短信")
    public Result<CaptchaInfos> captchaSms(@RequestParam(value = "phone") String phone,
                                                  @RequestParam(value = "captcha") String inputCaptcha, @RequestParam(value = "graphId") String graphId) {
        String captcha = captchaService.getCaptcha(CachesEnum.SmsCaptchaCache, graphId);
        if (StringUtils.equalsIgnoreCase(inputCaptcha, captcha)) {
            String uuid = UUID.randomUUID().toString();
            String smsCaptcha = RandomStringUtils.randomNumeric(6);
            captchaService.saveCaptcha(CachesEnum.SmsCaptchaCache, uuid, phone + "_" + smsCaptcha);
            log.info("smsCaptcha=" + smsCaptcha);
            // TODO send sms smsCaptcha
            CaptchaInfos captchaInfos = new CaptchaInfos();
            captchaInfos.setTtl(CachesEnum.SmsCaptchaCache.getTtl());
            captchaInfos.setGraphId(uuid);
            captchaService.removeCaptcha(CachesEnum.GraphCaptchaCache, graphId);
            return Result.ok(captchaInfos);
        }
        return Result.error(AuthErrorCodeEnum.CAPTCHA_CODE_ERROR);
    }

    /**
     * 图形验init
     */
    @ResponseBody
    @GetMapping("/graph")
    @ApiOperation(value = "验证图形获取")
    public Result<CaptchaInfos> captchaGraph() {
        String uuid = UUID.randomUUID().toString();
        String captcha = VerificationCodeUtil.generateVerificationCode(4, null);
        CaptchaInfos captchaInfos = new CaptchaInfos();
        captchaInfos.setTtl(CachesEnum.GraphCaptchaCache.getTtl());
        captchaInfos.setGraphId(uuid);
        captchaInfos.setGraphUrl("/captcha/graph/print?graphId=" + uuid);
        captchaService.saveCaptcha(CachesEnum.GraphCaptchaCache, uuid, captcha);
        return Result.ok(captchaInfos);

    }
    /**
     * 图形验证码打印
     *
     * @param graphId 验证码编号
     * @param width   图片宽度
     * @param height  图片高度
     */
    @GetMapping("/graph/print")
    @ApiOperation(value = "验证图形展示")
    public void captchaGraphPrint(HttpServletResponse response,
                                  @RequestParam(value = "graphId") String graphId,
                                  @RequestParam(value = "w", defaultValue = "150") int width,
                                  @RequestParam(value = "h", defaultValue = "38") int height) throws IOException {

        String captcha = captchaService.getCaptcha(CachesEnum.GraphCaptchaCache, graphId);
        if (StringUtils.isBlank(captcha)) {
            captcha = "0000";
        }
        response.setContentType("image/png");
        response.setHeader("Cache-Control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        long time = System.currentTimeMillis();
        response.setDateHeader("Last-Modified", time);
        response.setDateHeader("Date", time);
        response.setDateHeader("Expires", time);
        ServletOutputStream stream = response.getOutputStream();
        VerificationCodeUtil.outputImage(width, height, stream, captcha);
        stream.flush();
        stream.close();

    }

    /**
     * 图形验证码Base64
     *
     * @param graphId 验证码编号
     * @param width   图片宽度
     * @param height  图片高度
     */
    @ResponseBody
    @GetMapping("/graph/base64")
    @ApiOperation(value = "验证图形base64展示")
    public Result<CaptchaInfos> captchaGraphBase64(@RequestParam(value = "graphId") String graphId, @RequestParam(value = "w", defaultValue = "150") int width,
                                                  @RequestParam(value = "h", defaultValue = "38") int height) throws IOException {
        Map<String, Object> resultMap = new HashMap<>(16);
        String captcha = captchaService.getCaptcha(CachesEnum.GraphCaptchaCache, graphId);
        if (captcha != null) {
            String base64EncodedGraph = VerificationCodeUtil.outputImage(width, height, captcha);
            CaptchaInfos captchaInfos = new CaptchaInfos();
            captchaInfos.setBase64EncodedGraph(base64EncodedGraph);
            if("dev".equals(active)){
                captchaInfos.setCaptcha(captcha);
            }
            return Result.ok(captchaInfos);
        }
        return Result.error(AuthErrorCodeEnum.CAPTCHA_CODE_ERROR);
    }
}
