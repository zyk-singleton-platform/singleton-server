//package com.singleton.server.security.controller;
//
//import com.singleton.server.common.result.Result;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@Api(tags = "测试")
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//    @GetMapping("/hello")
//    @ApiOperation("Hello")
//    public Result<String> hello() {
//        return Result.ok("hello world");
//    }
//
//    @Secured("ROLE_SUPER")
//    @GetMapping("/super")
//    @ApiOperation("ROLE_SUPER 角色访问")
//    public Result<String> superRole() {
//        return Result.ok("hello world,super可以访问");
//    }
//
//    @Secured("ROLE_ADMIN")
//    @GetMapping("/admin")
//    @ApiOperation("ROLE_ADMIN 角色访问")
//    public Result<String> admin() {
//        return Result.ok("hello world,admin可以访问");
//    }
//
//    @PreAuthorize("hasRole('ROLE_ADMIN') AND hasRole('ROLE_EMPLOYEE')")
//    @GetMapping("/employee")
//    @ApiOperation("hasRole('ADMIN') AND hasRole('EMPLOYEE') 角色访问")
//    public Result<String> employee() {
//        return Result.ok("hello world,employee可以访问");
//    }
//}
