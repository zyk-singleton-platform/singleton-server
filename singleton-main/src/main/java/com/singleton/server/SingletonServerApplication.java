package com.singleton.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SingletonServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SingletonServerApplication.class, args);
    }
}
