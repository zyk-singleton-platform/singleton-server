package com.singleton.server.config.security.handler;

import com.singleton.server.common.result.Result;
import com.singleton.server.common.utils.ResponseUtil;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 匿名未登录的时候访问,需要登录的资源的调用类
 */
@Slf4j
@Component
public class LocalAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                         AuthenticationException authenticationException) throws IOException, ServletException {
        ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.REQUIRED_LOGIN_ERROR));
    }
}
