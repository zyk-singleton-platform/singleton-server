package com.singleton.server.config.security.store;

import com.alibaba.fastjson.JSON;
import com.singleton.server.common.component.CacheComponent;
import com.singleton.server.common.enums.CachesEnum;
import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.security.exception.AuthException;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
public class MemoryTokenStore implements ITokenStore{
    private CacheComponent cacheComponent;

    public MemoryTokenStore(CacheComponent cacheComponent) {
        this.cacheComponent = cacheComponent;
    }

    @Override
    public Token createdToken(TokenContext tokenContext) {
        Token token = new Token();
        String accessToken = UUID.randomUUID().toString();
        String refreshToken = UUID.randomUUID().toString();
        String accessTokenValue = JSON.toJSONString(tokenContext.toTokenMap());
        String refreshTokenValue = JSON.toJSONString(tokenContext.toRefreshTokenMap());
        boolean success = cacheComponent.saveCache(CachesEnum.TokenCache, accessToken, accessTokenValue);
        success = success && cacheComponent.saveCache(CachesEnum.RefreshTokenCache, refreshToken, refreshTokenValue);
        String account = tokenContext.getAccount();
        token.setAccessToken(accessToken);
        token.setRefreshToken(refreshToken);
        token.setSubject(account);
        String value = JSON.toJSONString(token);
        remove(account);
        success = success && cacheComponent.saveCache(CachesEnum.RefreshAccessTokenCache, account, value);
        if(!success){
            throw new AuthException(AuthErrorCodeEnum.CREATED_ACCESS_TOKEN_ERROR);
        }
        return token;
    }

    @Override
    public TokenContext getTokenContext(String token) {
        TokenContext tokenContext = null;
        try {
            String json= cacheComponent.getCacheString(CachesEnum.TokenCache, token.trim());
            tokenContext = JSON.parseObject(json, TokenContext.class);
        } catch (Exception e) {
            log.error("[token验证失败]", e);
        }
        if(tokenContext != null && tokenContext.getRefresh()){
            return null;
        }
        return tokenContext;
    }

    @Override
    public TokenContext getRefreshTokenContext(String refreshToken) {
        TokenContext refreshTokenContext = null;
        try {
            String json= cacheComponent.getCacheString(CachesEnum.RefreshTokenCache, refreshToken.trim());
            refreshTokenContext = JSON.parseObject(json, TokenContext.class);
        } catch (Exception e) {
            log.error("[RefreshToken验证失败]", e);
        }
        if(refreshTokenContext != null && refreshTokenContext.getRefresh()){
            return refreshTokenContext;
        }
        return null;
    }

    @Override
    public Boolean remove(String subject) {
        String json= cacheComponent.getCacheString(CachesEnum.RefreshAccessTokenCache, subject);
        Token subjectToken = JSON.parseObject(json, Token.class);
        if(subjectToken == null){
            return Boolean.TRUE;
        }
        cacheComponent.remove(CachesEnum.TokenCache, subjectToken.getAccessToken());
        cacheComponent.remove(CachesEnum.RefreshTokenCache, subjectToken.getRefreshToken());
        cacheComponent.remove(CachesEnum.RefreshAccessTokenCache, subject);
        return Boolean.TRUE;
    }
}
