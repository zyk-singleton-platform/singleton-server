package com.singleton.server.config.security.handler;

import com.singleton.server.common.result.Result;
import com.singleton.server.common.utils.ResponseUtil;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 没有权限,被拒绝访问时的调用类
 */
@Slf4j
@Component
public class LocalRestAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException, ServletException {
        log.error("Authentication Denied, Reason: " + accessDeniedException.getMessage());
        ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.PERMISSION_DENY_ERROR));
    }
}
