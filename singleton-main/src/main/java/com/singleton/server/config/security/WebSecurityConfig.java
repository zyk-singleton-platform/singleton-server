package com.singleton.server.config.security;

import com.singleton.server.common.component.CacheComponent;
import com.singleton.server.common.component.PasswordEncoderComponent;
import com.singleton.server.config.security.filter.JWTAuthenticationFilter;
import com.singleton.server.config.security.filter.LocalJwtAuthenticationTokenFilter;
import com.singleton.server.config.security.filter.LocalRolePermissionFilter;
import com.singleton.server.config.security.handler.*;
import com.singleton.server.config.security.properties.AuthProperties;
import com.singleton.server.config.security.properties.IgnoreUrlsProperties;
import com.singleton.server.config.security.provider.LoginAuthenticationProvider;
import com.singleton.server.config.security.store.ITokenStore;
import com.singleton.server.config.security.store.JwtTokenStore;
import com.singleton.server.config.security.store.MemoryTokenStore;
import com.singleton.server.security.service.LocalUserDetailsService;
import com.singleton.server.system.component.RolePermissionComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * @Author: zgd
 * @Date: 2019/1/15 17:42
 * @Description:
 */
@Configuration
@EnableWebSecurity
// 控制@Secured权限注解
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 这里需要交给spring注入,而不是直接new
     */
    @Autowired
    private IgnoreUrlsProperties ignoreUrlsProperties;
    @Autowired
    private PasswordEncoderComponent passwordEncoder;
    @Autowired
    private LocalUserDetailsService localUserDetailsService;
    @Autowired
    private LocalAuthenticationFailHandler localAuthenticationFailHandler;
    @Autowired
    private LocalAuthenticationSuccessHandler localAuthenticationSuccessHandler;
    @Autowired
    private LocalRestAccessDeniedHandler localRestAccessDeniedHandler;
    @Autowired
    private LocalLogoutSuccessHandler localLogoutSuccessHandler;
    @Autowired
    private LocalAuthenticationEntryPoint localAuthenticationEntryPoint;
    @Autowired
    private LoginAuthenticationProvider loginAuthenticationProvider;
    @Autowired
    private AuthProperties authProperties;
    @Autowired
    private CacheComponent cacheComponent;
    @Autowired
    private RolePermissionComponent rolePermissionComponent;

    @Bean
    public ITokenStore tokenStore() {
        AuthProperties cusAuthProperties = authProperties;
        if(cusAuthProperties == null){
            cusAuthProperties = new AuthProperties();
        }
        if(cusAuthProperties.getType().equals("jwt")){
            return new JwtTokenStore(authProperties);
        }
        return new MemoryTokenStore(cacheComponent);
    }

    /**
     * 该方法定义认证用户信息获取的来源、密码校验的规则
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 自定义密码校验的规则
        auth.authenticationProvider(loginAuthenticationProvider);
        //如果需要改变认证的用户信息来源，我们可以实现UserDetailsService
        auth.userDetailsService(localUserDetailsService).passwordEncoder(passwordEncoder);
        //inMemoryAuthentication 从内存中获取
        auth.inMemoryAuthentication()
                //spring security5 以上必须配置加密
                .passwordEncoder(passwordEncoder);
    }


    /**
     * Spring Security两种资源放行策略, 通过HttpSecurity这种方式过虑是走Spring Security过虑器链，在过虑器链中给请求放行。
     * 有的资源放行是必须要走HttpSecurity这种方式的，比如API登录接口这种非静态资源，因为在过虑过程中还有其他事情要做。
     * antMatchers: ant的通配符规则
     * ?	匹配任何单字符
     * *	匹配0或者任意数量的字符，不包含"/"
     * **	匹配0或者更多的目录，包含"/"
     * 要用Security的密码加盐算法必须要写这部分
     * authorize：授权
     * authenticated：认证
     * authorizeRequests所有security全注解配置实现的开端，表示说明开始需要的权限
     * 需要的权限分两部分：第一部分是拦截的路径，第二部分是访问该路径需要的权限
     * antMatchers：拦截路径"/**所有路径"，permitAll()：任何权限都可以直接通行
     * anyRequest:任何的请求，authenticated认证后才可以访问
     * .and().csrf().disable();固定写法，表示使csrf（一种网络攻击技术）拦截失效
     * 测试用资源，需要验证了的用户才能访问
     * 其他都放行了
     * .anyRequest().permitAll()
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         * antMatchers: ant的通配符规则
         * ? 匹配任何单字符
         * * 匹配0或者任意数量的字符，不包含"/"
         * ** 匹配0或者更多的目录，包含"/"
         */
        http.headers().frameOptions().disable();
        http.exceptionHandling()
                //登录后,访问没有权限处理类
                .accessDeniedHandler(localRestAccessDeniedHandler)
                //匿名访问,没有权限的处理类
                .authenticationEntryPoint(localAuthenticationEntryPoint);

        // 将自定义的OncePerRequestFilter过虑器加入到Security执行链中，解析过来的请求是否有token
        // 使用jwt的Authentication,来解析过来的请求是否有token
        http.addFilterBefore(new LocalJwtAuthenticationTokenFilter(authProperties, tokenStore()), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(new LocalRolePermissionFilter(ignoreUrlsProperties, rolePermissionComponent), FilterSecurityInterceptor.class);

        //这里表示不需要权限校验
        String[] ignoreApiUrls = ignoreUrlsProperties.getUrls().stream().toArray(String[]::new);
//        String[] ignoreResourcesUrls = ignoreUrlsProperties.getResources().stream().toArray(String[]::new);

        //对请求的授权
        http.authorizeRequests()
                .antMatchers(ignoreApiUrls).permitAll()
//                .antMatchers(ignoreResourcesUrls).permitAll()
                .anyRequest().authenticated()
                // 这里表示任何请求都需要校验认证(上面配置的放行)
                .and()
                //配置登录,检测到用户未登录时跳转的url地址,登录放行
                .formLogin()

                // 配置登录页面 在 Spring Security 中，如果我们不做任何配置，默认的登录页面和登录接口的地址都是 /login，也就是说，默认会存在如下两个请求：
                // GET http://localhost:8089/login
                // POST http://localhost:8089/login
                // 当我们配置了 loginPage 为 /login.html 之后，这个配置从字面上理解，就是设置登录页面的地址为 /login.html。
                // 实际上它还有一个隐藏的操作，就是登录接口地址也设置成 /login.html 了。
                // 换句话说，新的登录页面和登录接口地址都是 /login.html，现在存在如下两个请求：
                // GET http://localhost:8089/login.html
                // POST http://localhost:8089/login.html
                // 前面的 GET 请求用来获取登录页面，后面的 POST 请求用来提交登录数据。
                //.loginPage("/login.html")
                // 在 SecurityConfig 中，我们可以通过 loginProcessingUrl 方法来指定登录接口地址,这样配置之后，登录页面地址和登录接口地址就分开了，各是各的。
                //.loginProcessingUrl("/doLogin")
                // 使用forward的方式，能拿到具体失败的原因,并且会将错误信息以SPRING_SECURITY_LAST_EXCEPTION的key的形式将AuthenticationException
                // 对象保存到request域中
                //.failureForwardUrl("/sys/loginFail")
                // 如果直接访问登录页面，则登录成功后重定向到这个页面，否则跳转到之前想要访问的页面.
                //.defaultSuccessUrl("/public/login/ok.html")

                //需要跟前端表单的action地址一致
//                .loginProcessingUrl("/token/login")
                .successHandler(localAuthenticationSuccessHandler)
                .failureHandler(localAuthenticationFailHandler)
                .permitAll()
                //配置取消session管理,又Jwt来获取用户状态,否则即使token无效,也会有session信息,依旧判断用户为登录状态
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                //配置登出,登出放行
                .and()
                .logout()
                .logoutSuccessHandler(localLogoutSuccessHandler)
                .permitAll()
                .and()
                // 禁用csrf模式
                .csrf().disable();
    }

    /**
     * Spring Security 两种资源放行策略
     * 通过WebSecurity这种方法不走Spring Security过虑器链，通常静态资源可以使用这种方式过虑放行，因为这些资源不需要权限就可以访问。
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        //权限控制需要忽略所有静态资源，不然登录页面未登录状态无法加载css等静态资源
        ignoreUrlsProperties.getResources().forEach(resource -> web.ignoring().antMatchers(resource));
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

    /**
     * 将JWTAuthenticationFilter过虑器注册到容器中
     *
     * @return
     * @throws Exception
     */
    @Bean
    public JWTAuthenticationFilter jwtAuthenticationFilter() throws Exception {
        JWTAuthenticationFilter filter = new JWTAuthenticationFilter(authenticationManagerBean());
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.setAuthenticationSuccessHandler(localAuthenticationSuccessHandler);
        filter.setAuthenticationFailureHandler(localAuthenticationFailHandler);
        return filter;
    }
}