package com.singleton.server.config.security.provider;

import com.alibaba.fastjson.JSON;
import com.singleton.server.common.component.PasswordEncoderComponent;
import com.singleton.server.common.enums.CachesEnum;
import com.singleton.server.security.domain.LocalUsersssDetails;
import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.security.exception.AuthException;
import com.singleton.server.security.service.CaptchaService;
import com.singleton.server.security.service.LocalUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


/**
 *
 * 自定义密码校验的规则
 */
@Slf4j
@Component
public class LoginAuthenticationProvider implements AuthenticationProvider {

    public static final String VALIDATE_CODE = "validateCode";
    public static final String GRAPH_ID = "graphId";

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    LocalUserDetailsService userDetailsService;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    PasswordEncoderComponent passwordEncoderComponent;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 获取前端表单中输入后返回的用户名、密码
        String userName = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        Object details = authentication.getDetails();
        System.out.println(JSON.toJSONString(details));
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
        LocalUsersssDetails customerUserDetails = (LocalUsersssDetails) userDetails;

        String graphId =  obtainGraphIdParameter();
        String captchaValidateCode = captchaService.getCaptcha(CachesEnum.GraphCaptchaCache, graphId);
        if (StringUtils.isEmpty(captchaValidateCode)) {
            throw new AuthException(AuthErrorCodeEnum.CAPTCHA_CODE_INVALID);
        }
        log.info("password:{}, user:{}, captchaValidateCode:{}",password, customerUserDetails.getPassword(), captchaValidateCode.toUpperCase());
        if (!passwordEncoderComponent.matches(password, customerUserDetails.getPassword(), captchaValidateCode.toUpperCase())) {
            throw new AuthException(AuthErrorCodeEnum.CAPTCHA_CODE_OR_PASSWORD_ERROR);
        }

        TokenContext tokenContext = new TokenContext(customerUserDetails.getRoleCodes(), customerUserDetails.getUsername(), customerUserDetails.getId(), customerUserDetails.isEnabled(), customerUserDetails.getAccount(), customerUserDetails.getPhone(), customerUserDetails.getEmail());

        captchaService.removeCaptcha(CachesEnum.GraphCaptchaCache, graphId);
        return new UsernamePasswordAuthenticationToken(tokenContext, userDetails.getPassword(), userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }


    protected String obtainValidateCodeParameter() {
        Object object = httpServletRequest.getParameter(VALIDATE_CODE);
        return null == object ? "" : object.toString();
    }


    protected String obtainGraphIdParameter() {
        Object object = httpServletRequest.getParameter(GRAPH_ID);
        return null == object ? "" : object.toString();
    }
}