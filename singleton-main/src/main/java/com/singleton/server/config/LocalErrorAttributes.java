package com.singleton.server.config;

import com.alibaba.fastjson.JSON;
import com.singleton.server.common.basic.BasicException;
import com.singleton.server.common.enums.CommonErrorCodeEnum;
import com.singleton.server.common.result.Result;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.ServletException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

@Configuration
public class LocalErrorAttributes extends DefaultErrorAttributes {
    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
        Throwable error = getError(webRequest);
        if (error != null) {
            while (error instanceof ServletException && error.getCause() != null) {
                error = error.getCause();
            }
        }
        if (error != null) {
            Result<Object> result = null;
            if (error instanceof BasicException) {
                result = Result.error((BasicException) error);
            }else if (error instanceof Exception) {
                result = Result.error(CommonErrorCodeEnum.SERVER_ERROR);
            }
            if (result != null){
                StringWriter stackTrace = new StringWriter();
                error.printStackTrace(new PrintWriter(stackTrace));
                stackTrace.flush();
                result.setStack(stackTrace.toString());
                return JSON.parseObject(JSON.toJSONString(result)).getInnerMap();
            }
        }
        return super.getErrorAttributes(webRequest, options);
    }
}
