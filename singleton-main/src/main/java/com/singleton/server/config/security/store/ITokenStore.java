package com.singleton.server.config.security.store;

import com.singleton.server.common.domain.TokenContext;
import lombok.Data;

public interface ITokenStore {
    Token createdToken(TokenContext tokenContext);
    TokenContext getTokenContext(String token);
    TokenContext getRefreshTokenContext(String refreshToken);
    Boolean remove(String subject);
    @Data
    public static class Token{
        private String subject;
        private String accessToken;
        private String refreshToken;
    }
}
