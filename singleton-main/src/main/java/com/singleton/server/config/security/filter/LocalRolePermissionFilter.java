package com.singleton.server.config.security.filter;

import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.common.result.Result;
import com.singleton.server.common.utils.ResponseUtil;
import com.singleton.server.config.security.properties.IgnoreUrlsProperties;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import com.singleton.server.system.component.RolePermissionComponent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 角色权限过滤器
 */
@Slf4j
public class LocalRolePermissionFilter implements Filter {
    /**
     * 接口资源
     */
    private List<String> urls = new ArrayList<>();

    private RolePermissionComponent rolePermissionComponent;

    public LocalRolePermissionFilter(IgnoreUrlsProperties ignoreUrlsProperties, RolePermissionComponent rolePermissionComponent) {
        this.urls.add("/error");
        this.urls.addAll(ignoreUrlsProperties.getUrls());
        this.urls.addAll(ignoreUrlsProperties.getResources());
        this.urls.addAll(ignoreUrlsProperties.getEveryOneApis());
        this.rolePermissionComponent = rolePermissionComponent;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String servletPath = ((HttpServletRequest) request).getServletPath();
        if(verifyPathHasProperties(servletPath)){
            chain.doFilter(request, response);
        } else{
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if(buildNullErrorResponse(authentication, response)){
                log.error("authentication is null");
                return;
            }
            Collection<? extends GrantedAuthority> authorities = null;
            if(authentication instanceof AnonymousAuthenticationToken){
                authorities = authentication.getAuthorities();
            }
            if(authentication instanceof UsernamePasswordAuthenticationToken){
                TokenContext tokenContext = (TokenContext)authentication.getPrincipal();
                if(buildNullErrorResponse(tokenContext, response)){
                    log.error("tokenContext is null");
                    return;
                }
                authorities = tokenContext.getAuthorities();
            }
            if(buildEmptyErrorResponse(authorities, response)){
                log.error("authorities is null");
                return;
            }
            List<String> roleCodes = authorities.stream().map(GrantedAuthority::getAuthority).filter(StringUtils::isNotEmpty).distinct().collect(Collectors.toList());
            if(buildEmptyErrorResponse(roleCodes, response)){
                log.error("roleCodes is null");
                return;
            }
            List<String> permissions = this.rolePermissionComponent.getPermissions(roleCodes);
            if(buildEmptyErrorResponse(permissions, response)){
                log.error("permissions is null");
                return;
            }
            if(permissions.contains(servletPath)){
                chain.doFilter(request, response);
            }else{
                ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.ROLE_API_AUTH_ERROR));
            }
        }
    }

    private boolean buildEmptyErrorResponse(Collection<?> collection, ServletResponse response) {
        if(CollectionUtils.isEmpty(collection)){
            ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.ROLE_API_AUTH_ERROR));
            return true;
        }
        return false;
    }

    private boolean buildNullErrorResponse(Object obj, ServletResponse response) {
        if(obj == null){
            ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.ROLE_API_AUTH_ERROR));
            return true;
        }
        return false;
    }


    private boolean verifyPathHasProperties(String path) {
        for (String url : this.urls) {
            if (Pattern.matches(buildRegexUrl(url), path)) {
                return true;
            }
        }
        return false;
    }

    private String buildRegexUrl(String url) {
        return url.replace(".", "\\.").replace("**", "[\\w\\./]*");
    }
}
