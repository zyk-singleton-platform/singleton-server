package com.singleton.server.config.security.filter;

import com.singleton.server.security.service.CaptchaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 验证用户登录信息的拦截器 UsernamePasswordAuthenticationFilter拦截登陆请求
 * 当我们想要在 Spring Security 自定义一个登录验证码或者将登录参数改为 JSON 的时候
 * 我们都需自定义过滤器继承自 AbstractAuthenticationProcessingFilter
 * 毫无疑问UsernamePasswordAuthenticationFilter#attemptAuthentication 方法
 * 就是在 AbstractAuthenticationProcessingFilter 类的 doFilter 方法中被触发的
 */
@Slf4j
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    @Autowired
    private CaptchaService captchaService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        super.setFilterProcessesUrl("/token/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        //检测验证码是否正确
        return super.attemptAuthentication(request, response);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response, FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
}