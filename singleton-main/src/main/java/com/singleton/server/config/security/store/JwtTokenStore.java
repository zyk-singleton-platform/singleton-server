package com.singleton.server.config.security.store;

import com.singleton.server.common.utils.JwtTokenUtil;
import com.singleton.server.config.security.properties.AuthProperties;
import com.singleton.server.common.domain.TokenContext;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Slf4j
public class JwtTokenStore implements ITokenStore{
    private RSAPublicKey publicKey = null;
    private RSAPrivateKey privateKey = null;
    private AuthProperties authProperties;

    public JwtTokenStore(AuthProperties authProperties) {
        this.authProperties = authProperties;
    }

    @Override
    public Token createdToken(TokenContext tokenContext) {
        Token token = new Token();
        String accessToken = JwtTokenUtil.generateToken(tokenContext.getAccount(), tokenContext.toTokenMap(), authProperties.getExpiration());
        String refreshToken = JwtTokenUtil.generateToken(tokenContext.getAccount(), tokenContext.toRefreshTokenMap(), authProperties.getRefreshExpiration());
        String account = tokenContext.getAccount();
        token.setAccessToken(accessToken);
        token.setRefreshToken(refreshToken);
        token.setSubject(account);
        return token;
    }

    @Override
    public TokenContext getTokenContext(String token) {
        TokenContext tokenContext = null;
        try {
            Claims claims = JwtTokenUtil.parseToken(token.trim(), publicKey, privateKey);
            tokenContext = new TokenContext(claims);
        } catch (Exception e) {
            log.error("[token验证失败]", e);
        }
        if(tokenContext != null && tokenContext.getRefresh()){
            return null;
        }
        return tokenContext;
    }

    @Override
    public TokenContext getRefreshTokenContext(String refreshToken) {
        TokenContext refreshTokenContext = null;
        try {
            Claims claims = JwtTokenUtil.parseToken(refreshToken.trim(), publicKey, privateKey);
            refreshTokenContext = new TokenContext(claims);
        } catch (Exception e) {
            log.error("[token验证失败]", e);
        }
        if(refreshTokenContext != null && refreshTokenContext.getRefresh()){
            return refreshTokenContext;
        }
        return null;
    }

    @Override
    public Boolean remove(String subject) {
        return Boolean.TRUE;
    }
}
