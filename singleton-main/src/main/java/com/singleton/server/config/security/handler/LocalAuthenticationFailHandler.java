package com.singleton.server.config.security.handler;

import com.singleton.server.common.result.Result;
import com.singleton.server.common.utils.ResponseUtil;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录账号密码错误等情况下,会调用的处理类
 */
@Slf4j
@Component
public class LocalAuthenticationFailHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException authenticationException) throws IOException, ServletException {
        ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.LOGIN_UNMATCH_ERROR));
    }
}
