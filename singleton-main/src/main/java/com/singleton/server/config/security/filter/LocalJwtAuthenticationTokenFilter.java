package com.singleton.server.config.security.filter;

import com.singleton.server.common.result.Result;
import com.singleton.server.common.utils.ResponseUtil;
import com.singleton.server.config.security.properties.AuthProperties;
import com.singleton.server.config.security.store.ITokenStore;
import com.singleton.server.common.domain.TokenContext;
import com.singleton.server.security.enums.AuthErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 定义我们自己的JWT拦截器，在请求到达目标之前对Token进行校验
 * 在请求过来的时候,解析请求头中的token,再解析token得到用户信息,再存到SecurityContextHolder中
 */
@Slf4j
public class LocalJwtAuthenticationTokenFilter extends OncePerRequestFilter {
    private AuthProperties userAuthProperties;
    private ITokenStore tokenStore;
    public LocalJwtAuthenticationTokenFilter(AuthProperties userAuthProperties, ITokenStore tokenStore) {
        this.userAuthProperties = userAuthProperties;
        this.tokenStore = tokenStore;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authHeader = request.getHeader(userAuthProperties.getTokenHeader());
        if (authHeader != null && authHeader.startsWith(userAuthProperties.getTokenPrefix())) {
            //请求头有token
            final String authToken = authHeader.substring(userAuthProperties.getTokenPrefix().length());

            TokenContext tokenContext;
            try {
                tokenContext = tokenStore.getTokenContext(authToken);
                if(tokenContext == null){
                    log.info("token解析失败");
                    ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.REQUIRED_LOGIN_ERROR));
                    return;
                }
            } catch (Exception exception) {
                ResponseUtil.out(HttpStatus.OK.value(), Result.error(AuthErrorCodeEnum.REQUIRED_LOGIN_ERROR));
                return;
            }
            UserDetails userDetails = tokenContext;
            if (tokenContext.getAccount() != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                if (userDetails != null) {
                    //必须token解析的时间戳和session保存的一致
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    //如果有accessToken的请求头，取出token，解析token，解析成功说明token正确，将解析出来的用户信息放到SpringSecurity的上下文中
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        chain.doFilter(request, response);
    }
}
