package com.singleton.server.config.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "auth")
public class AuthProperties {
    /**
     * #jwt、memory
     */
    private String type = "jwt";

    /**
     * #JWT存储的请求头
     */
    private String tokenHeader = "Authorization";

    /**
     *#JWT加解密使用的密钥
     */
    private String secret;

    /**
     * #token的超期限时间(60*60*24)
     */
    private long expiration = 604800;

    /**
     * #refresh-token的超期限时间(60*60*24*2)
     */
    private long refreshExpiration = 1209600;

    /**
     * #JWT负载中拿到开头
     */
    private String tokenPrefix = "Bearer";
}
